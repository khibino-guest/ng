Source: ng
Section: editors
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends:
 debhelper-compat (= 13),
 libcanna1g-dev,
 libncurses-dev,
Standards-Version: 4.6.0
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/debian/ng
Vcs-Git: https://salsa.debian.org/debian/ng.git

Package: ng-common
Architecture: all
Depends: ${misc:Depends}
Recommends: ng-latin | ng-cjk | ng-cjk-canna
Conflicts: ng
Replaces: ng
Description: Common files used by ng-* packages
 Ng is Nihongo Mg, MicroGnuEmacs. It is a small lightweight Emacs-like
 editor. It can handle both Latin and CJK.  UTF-8 is now supported.
 .
 This package contains documents and a wrapper script.

Package: ng-latin
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, ng-common
Description: Nihongo MicroGnuEmacs with Latin support
 Ng is Nihongo Mg, MicroGnuEmacs. It is a small lightweight Emacs-like
 editor. It can handle both Latin and CJK.
 .
 ng-latin can handle Latin (ISO-8859) encoding. CJK is not supported.
 UTF-8 is now supported.

Package: ng-cjk
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, ng-common
Description: Nihongo MicroGnuEmacs with CJK support
 Ng is Nihongo Mg, MicroGnuEmacs. It is a small lightweight Emacs-like
 editor. It can handle both Latin and CJK.
 .
 ng-cjk can handle ISO-2022-JP, Shift-JIS, EUC-JP as well as EUC-KR and
 EUC-CN(GB only). Latin is not supported. UTF-8 is now supported.

Package: ng-cjk-canna
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, ng-common
Description: Nihongo MicroGnuEmacs with CJK and Canna support
 Ng is Nihongo Mg, MicroGnuEmacs. It is a small lightweight Emacs-like
 editor. It can handle both Latin and CJK.
 .
 ng-cjk-canna can handle ISO-2022-JP, Shift-JIS, EUC-JP as well as EUC-KR and
 EUC-CN(GB only). Latin is not supported. UTF-8 is now supported. Canna,
 one of Japanese input methods, is also supported.
